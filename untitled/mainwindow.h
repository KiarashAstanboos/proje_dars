#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include<QMediaPlayer>

#include"home_window.h"
#include"tas_window.h"
#include"player_class.h"
#include"melk_class.h"
#include"action_window.h"
#include"ooj.h"
#include"sell_window.h"

#include<QLabel>
#include<QVector>
#include<QTimer>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    int ingame_players; //tedad player haii ke hanooz nabakhtan

private slots:

    void update_Status();//status player haro update mikone
    void on_pushButton_clicked(); // pushbutton start
    void on_pushButton_2_clicked(); //push button home
    void on_pushButton_3_clicked();// role dice

    void on_pushButton_4_clicked();

    void on_Mortgage_btn_clicked();

    void on_build_btn_clicked();

    void on_ooj_btn_clicked();

    void on_sell_btn_clicked();

    void on_pushButton_5_clicked();

private:
    Home_window *H;
    tas_window *tw;
    action_window*aw;
    ooj *oojw;
    sell_window *sellw;

    QVector<QLabel*> labels;

    Ui::MainWindow *ui;
    melk Board[40];
    Player players[8];
    int doubl=0;//baraye taas ke chand bar poshte ham double shode

    int tedad_player;
    int taas;// har dafe tas andakhte beshe az class tas rikhte mishe too in
    int nobat=0;// baraye moshakhas kardan nobat dar aval game
    int turn=0; // baraye nobat dar toole game


    QLabel *mohre[8];
    QLabel *rang[8];
    QTimer *update;
    QMediaPlayer *background;
    void varshekastegi(int tartib);
};



#endif // MAINWINDOW_H
