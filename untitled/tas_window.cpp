#include "tas_window.h"
#include "ui_tas_window.h"
#include<QTime>
#include<QMessageBox>
tas_window::tas_window(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::tas_window)
{

    ui->setupUi(this);

    Dice1=  rand()%6+1;
    srand((unsigned)time(NULL));
    Dice2=  rand() %6+1;

    tas=Dice1+Dice2;
    ui->tas1_Val->setNum(Dice1);
    ui->tas2_Val->setNum(Dice2);
    if(Dice1==Dice2)
    {
        QMessageBox::information(this,"DOUBLE","DOUBLE");
    }
    hide();

}

tas_window::~tas_window()
{
    delete ui;
}




void tas_window::on_okay_btn_clicked()
{
    hide();
}

void tas_window::delay()
{

    QTime dieTime= QTime::currentTime().addSecs(1);
    while (QTime::currentTime() < dieTime)
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);

}
