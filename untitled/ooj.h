#ifndef OOJ_H
#define OOJ_H

#include <QDialog>
#include "player_class.h"
#include"tas_window.h"
namespace Ui {
class ooj;
}

class ooj : public QDialog
{
    Q_OBJECT

public:
    explicit ooj(QWidget *parent = nullptr);
    ~ooj();
void getout(Player &player);

private slots:

    void on_pay_clicked();

    void on_card_clicked();

    void on_roll_clicked();

private:
    Player *player_cp;
    tas_window *twj;
    Ui::ooj *ui;
    int taas;
};

#endif // OOJ_H
