#ifndef SELL_WINDOW_H
#define SELL_WINDOW_H
#include "melk_class.h"
#include <QDialog>

namespace Ui {
class sell_window;
}

class sell_window : public QDialog
{
    Q_OBJECT

public:
    explicit sell_window(QWidget *parent = nullptr);
    ~sell_window();
melk *Board_C;
Player *Players_c;

private slots:
    void on_pushButton_2_clicked();

private:
    void sell(QWidget *parent,melk Board[],Player &player,int price,int block);
    Ui::sell_window *ui;
    int price;
    int block;
    int player_pos;

};

#endif // SELL_WINDOW_H
