#ifndef BOARD_H
#define BOARD_H
#include"melk_class.h"
void define_board(melk array[])//array az melk migire va board ro misaze
{
    //GO            '0'
    //chance:       '7' '22' '36'
    //community     '2' '17' '33'
    //jail          '10'
    //go to jail    '30'
    // pay 200      '4'
    // pay 75       '38'
    // free parking '20'
    array[1].rent=2;
    array[1].rent_default=2;
    array[1].rent_1=10;
    array[1].rent_2=30;
    array[1].rent_3=90;
    array[1].rent_4=160;
    array[1].rent_hotel=250;
    array[1].mortgage=30;
    array[1].house_cost=50;
    array[1].hotel_cost=50;
    array[1].price=60;

    array[3].rent=4;
    array[3].rent_default=4;
    array[3].rent_1=20;
    array[3].rent_2=60;
    array[3].rent_3=180;
    array[3].rent_4=320;
    array[3].rent_hotel=450;
    array[3].mortgage=30;
    array[3].house_cost=50;
    array[3].hotel_cost=50;
    array[3].price=60;

    //RR
    array[5].rent=25;
    array[5].rent_2=50;
    array[5].rent_3=100;
    array[5].rent_4=200;
    array[5].mortgage=100;
    array[5].price=200;

    array[6].rent=6;
    array[6].rent_default=6;
    array[6].rent_1=30;
    array[6].rent_2=90;
    array[6].rent_3=270;
    array[6].rent_4=400;
    array[6].rent_hotel=550;
    array[6].mortgage=50;
    array[6].house_cost=50;
    array[6].hotel_cost=50;
    array[6].price=100;

    array[8].rent=6;
    array[8].rent_default=6;
    array[8].rent_1=30;
    array[8].rent_2=90;
    array[8].rent_3=270;
    array[8].rent_4=400;
    array[8].rent_hotel=550;
    array[8].mortgage=50;
    array[8].house_cost=50;
    array[8].hotel_cost=50;
    array[8].price=100;

    array[9].rent=8;
    array[9].rent_default=8;
    array[9].rent_1=40;
    array[9].rent_2=100;
    array[9].rent_3=300;
    array[9].rent_4=450;
    array[9].rent_hotel=600;
    array[9].mortgage=60;
    array[9].house_cost=50;
    array[9].hotel_cost=50;
    array[9].price=120;

    array[11].rent=10;
    array[11].rent_default=10;
    array[11].rent_1=50;
    array[11].rent_2=150;
    array[11].rent_3=450;
    array[11].rent_4=625;
    array[11].rent_hotel=750;
    array[11].mortgage=70;
    array[11].house_cost=100;
    array[11].hotel_cost=100;
    array[11].price=140;

    //electric company
    array[12].rent=4; //*dice
    array[12].rent_2=10; //*dice
    array[12].mortgage=75;
    array[12].price=150;

    array[13].rent=10;
    array[13].rent_default=10;
    array[13].rent_1=50;
    array[13].rent_2=150;
    array[13].rent_3=450;
    array[13].rent_4=625;
    array[13].rent_hotel=750;
    array[13].mortgage=70;
    array[13].house_cost=100;
    array[13].hotel_cost=100;
    array[13].price=140;

    array[14].rent=12;
    array[14].rent_default=12;
    array[14].rent_1=60;
    array[14].rent_2=180;
    array[14].rent_3=500;
    array[14].rent_4=700;
    array[14].rent_hotel=900;
    array[14].mortgage=80;
    array[14].house_cost=100;
    array[14].hotel_cost=100;
    array[14].price=160;

    //RR
    array[15].rent=25;
    array[15].rent_2=50;
    array[15].rent_3=100;
    array[15].rent_4=200;
    array[15].mortgage=100;
    array[15].price=200;

    array[16].rent=14;
    array[16].rent_default=14;
    array[16].rent_1=70;
    array[16].rent_2=200;
    array[16].rent_3=550;
    array[16].rent_4=750;
    array[16].rent_hotel=950;
    array[16].mortgage=90;
    array[16].house_cost=100;
    array[16].hotel_cost=100;
    array[16].price=180;

    array[18].rent=14;
    array[18].rent_default=14;
    array[18].rent_1=70;
    array[18].rent_2=200;
    array[18].rent_3=550;
    array[18].rent_4=750;
    array[18].rent_hotel=950;
    array[18].mortgage=90;
    array[18].house_cost=100;
    array[18].hotel_cost=100;
    array[18].price=180;

    array[19].rent=16;
    array[19].rent_default=16;
    array[19].rent_1=80;
    array[19].rent_2=220;
    array[19].rent_3=600;
    array[19].rent_4=800;
    array[19].rent_hotel=1000;
    array[19].mortgage=100;
    array[19].house_cost=100;
    array[19].hotel_cost=100;
    array[19].price=200;

    array[21].rent=18;
    array[21].rent_default=18;
    array[21].rent_1=90;
    array[21].rent_2=250;
    array[21].rent_3=700;
    array[21].rent_4=875;
    array[21].rent_hotel=1050;
    array[21].mortgage=110;
    array[21].house_cost=150;
    array[21].hotel_cost=150;
    array[21].price=220;

    array[23].rent=18;
    array[23].rent_default=18;
    array[23].rent_1=90;
    array[23].rent_2=250;
    array[23].rent_3=700;
    array[23].rent_4=875;
    array[23].rent_hotel=1050;
    array[23].mortgage=110;
    array[23].house_cost=150;
    array[23].hotel_cost=150;
    array[23].price=220;

    array[24].rent=20;
    array[24].rent_default=20;
    array[24].rent_1=100;
    array[24].rent_2=300;
    array[24].rent_3=750;
    array[24].rent_4=925;
    array[24].rent_hotel=1100;
    array[24].mortgage=120;
    array[24].house_cost=150;
    array[24].hotel_cost=150;
    array[24].price=240;

    //RR
    array[25].rent=25;
    array[25].rent_2=50;
    array[25].rent_3=100;
    array[25].rent_4=200;
    array[25].mortgage=100;
    array[25].price=200;

    array[26].rent=22;
    array[26].rent_default=22;
    array[26].rent_1=110;
    array[26].rent_2=330;
    array[26].rent_3=800;
    array[26].rent_4=975;
    array[26].rent_hotel=1150;
    array[26].mortgage=130;
    array[26].house_cost=150;
    array[26].hotel_cost=150;
    array[26].price=260;

    array[27].rent=22;
    array[27].rent_default=22;
    array[27].rent_1=110;
    array[27].rent_2=330;
    array[27].rent_3=800;
    array[27].rent_4=975;
    array[27].rent_hotel=1150;
    array[27].mortgage=130;
    array[27].house_cost=150;
    array[27].hotel_cost=150;
    array[27].price=260;

    //Water works
    array[28].rent=4; //*dice
    array[28].rent_2=10; //*dice
    array[28].mortgage=75;
    array[28].price=150;

    array[29].rent=24;
    array[29].rent_default=24;
    array[29].rent_1=120;
    array[29].rent_2=360;
    array[29].rent_3=850;
    array[29].rent_4=1025;
    array[29].rent_hotel=1200;
    array[29].mortgage=140;
    array[29].house_cost=150;
    array[29].hotel_cost=150;
    array[29].price=280;

    array[31].rent=26;
    array[31].rent_default=26;
    array[31].rent_1=130;
    array[31].rent_2=390;
    array[31].rent_3=900;
    array[31].rent_4=1100;
    array[31].rent_hotel=1275;
    array[31].mortgage=150;
    array[31].house_cost=200;
    array[31].hotel_cost=200;
    array[31].price=300;

    array[32].rent=26;
    array[32].rent_default=26;
    array[32].rent_1=130;
    array[32].rent_2=390;
    array[32].rent_3=900;
    array[32].rent_4=1100;
    array[32].rent_hotel=1275;
    array[32].mortgage=150;
    array[32].house_cost=200;
    array[32].hotel_cost=200;
    array[32].price=300;


    array[34].rent=28;
    array[34].rent_default=28;
    array[34].rent_1=150;
    array[34].rent_2=450;
    array[34].rent_3=1000;
    array[34].rent_4=1200;
    array[34].rent_hotel=1400;
    array[34].mortgage=160;
    array[34].house_cost=200;
    array[34].hotel_cost=200;
    array[34].price=320;

    //RR
    array[35].rent=25;
    array[35].rent_2=50;
    array[35].rent_3=100;
    array[35].rent_4=200;
    array[35].mortgage=100;
    array[35].price=200;

    array[37].rent=35;
    array[37].rent_default=35;
    array[37].rent_1=175;
    array[37].rent_2=500;
    array[37].rent_3=1100;
    array[37].rent_4=1300;
    array[37].rent_hotel=1500;
    array[37].mortgage=175;
    array[37].house_cost=200;
    array[37].hotel_cost=200;
    array[37].price=350;


    array[39].rent=50;
    array[39].rent_default=50;
    array[39].rent_1=200;
    array[39].rent_2=600;
    array[39].rent_3=1400;
    array[39].rent_4=1700;
    array[39].rent_hotel=2000;
    array[39].mortgage=200;
    array[39].house_cost=200;
    array[39].hotel_cost=200;
    array[39].price=400;
}
#endif // BOARD_H
