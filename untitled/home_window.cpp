#include "home_window.h"
#include "ui_home_window.h"

Home_window::Home_window(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Home_window)
{
    ui->setupUi(this);
}

Home_window::~Home_window()
{
    delete ui;
}

void Home_window::on_pushButton_clicked()
{
    if(i==-1)//gereftan tedad player
    {
        tedad_player_home=ui->spinBox->value();
        i++;
        ui->spinBox->setEnabled(false);
        ui->lineEdit->setEnabled(true);
    }
    else//gereftan esme player ha
    {
        players_name_home[i]=ui->lineEdit->text();
        ui->lineEdit->clear();
        if(i==tedad_player_home-1) hide();
        ui->label->setText("Player" +  QString::number(i+2));
        i++;
    }
}
