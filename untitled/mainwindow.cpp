#include "mainwindow.h"
#include "ui_mainwindow.h"
#include<QMediaPlayer>
#include "board.h"
#include"mohre.h"
#include"Build.h"
#include<Qtime>
#include"sell_window.h"


#include"move.h"

#include<QMessageBox>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    H=new Home_window;

    // sakhtan araye label ke har label esme owner va tedad khune o hotel ro dare
    labels.push_back(ui->melk_0);
    labels.push_back(ui->melk_1);
    labels.push_back(ui->melk_2);
    labels.push_back(ui->melk_3);
    labels.push_back(ui->melk_4);
    labels.push_back(ui->melk_5);
    labels.push_back(ui->melk_6);
    labels.push_back(ui->melk_7);
    labels.push_back(ui->melk_8);
    labels.push_back(ui->melk_9);
    labels.push_back(ui->melk_10);
    labels.push_back(ui->melk_11);
    labels.push_back(ui->melk_12);
    labels.push_back(ui->melk_13);
    labels.push_back(ui->melk_14);
    labels.push_back(ui->melk_15);
    labels.push_back(ui->melk_16);
    labels.push_back(ui->melk_17);
    labels.push_back(ui->melk_18);
    labels.push_back(ui->melk_19);
    labels.push_back(ui->melk_20);
    labels.push_back(ui->melk_21);
    labels.push_back(ui->melk_22);
    labels.push_back(ui->melk_23);
    labels.push_back(ui->melk_24);
    labels.push_back(ui->melk_25);
    labels.push_back(ui->melk_26);
    labels.push_back(ui->melk_27);
    labels.push_back(ui->melk_28);
    labels.push_back(ui->melk_29);
    labels.push_back(ui->melk_30);
    labels.push_back(ui->melk_31);
    labels.push_back(ui->melk_32);
    labels.push_back(ui->melk_33);
    labels.push_back(ui->melk_34);
    labels.push_back(ui->melk_35);
    labels.push_back(ui->melk_36);
    labels.push_back(ui->melk_37);
    labels.push_back(ui->melk_38);
    labels.push_back(ui->melk_39);






    update = new QTimer(this);
    define_board(Board);

    ui->pushButton_4->hide();

    ui->sell_btn->hide();
    ui->build_btn->hide();
    ui->Mortgage_btn->hide();
    ui->ooj_btn->hide();
    ui->getblock->hide();
    ui->label_3->hide();

    //mohre player haro misaze
    generate(this,tedad_player,mohre,rang);
    for(int i=0;i<8;i++) rang[i]->hide();
    for(int i=0;i<8;i++) mohre[i]->hide();



}

MainWindow::~MainWindow()
{
    delete ui;

}
void MainWindow::update_Status()
{

    // pool player
    for(int i=0;i<tedad_player;i++)
    {
        if(players[i].out==false)
        {
            QString Text= "    "+players[i].Name + "   "+QString::number(players[i].Money)+"$";
            ui->listWidget->item(i)->setText(Text);
        }
    }

    //esme player miad roo melk haii ke kharide
    for(int j=0;j<40;j++)
        if(Board[j].owned==true)
            labels[j]->setText(Board[j].owner->Name +" "+ QString::number(Board[j].tedad_khune)+"h & "+QString::number(Board[j].tedad_hotel)+"H");

    // check mikone player enhesare rang dare ya na va age dare rent 2x she
    if(Board[1].owner==Board[3].owner)
    {
        ui->color_owner_1->setText(Board[3].owner->Name);
        Board[1].rent=2*Board[1].rent_default;
        Board[3].rent=2*Board[3].rent_default;
    }

    if(Board[6].owner==Board[8].owner && Board[6].owner==Board[9].owner)
    {
        ui->color_owner_2->setText(Board[6].owner->Name);
        Board[6].rent=2*Board[6].rent_default;
        Board[8].rent=2*Board[8].rent_default;
        Board[9].rent=2*Board[9].rent_default;

    }

    if(Board[11].owner==Board[13].owner && Board[11].owner==Board[14].owner)
    {

        ui->color_owner_3->setText(Board[11].owner->Name);
        Board[11].rent=2*Board[11].rent_default;
        Board[13].rent=2*Board[13].rent_default;
        Board[14].rent=2*Board[14].rent_default;

    }

    if(Board[16].owner==Board[18].owner && Board[16].owner==Board[19].owner)
    {
        ui->color_owner_4->setText(Board[16].owner->Name);
        Board[16].rent=2*Board[16].rent_default;
        Board[18].rent=2*Board[18].rent_default;
        Board[19].rent=2*Board[19].rent_default;

    }

    if(Board[21].owner==Board[23].owner && Board[21].owner==Board[24].owner)
    {
        ui->color_owner_5->setText(Board[21].owner->Name);
        Board[21].rent=2*Board[21].rent_default;
        Board[23].rent=2*Board[23].rent_default;
        Board[24].rent=2*Board[24].rent_default;
    }

    if(Board[26].owner==Board[27].owner && Board[26].owner==Board[29].owner)
    {
        ui->color_owner_6->setText(Board[26].owner->Name);
        Board[26].rent=2*Board[26].rent_default;
        Board[27].rent=2*Board[27].rent_default;
        Board[29].rent=2*Board[29].rent_default;

    }

    if(Board[31].owner==Board[32].owner && Board[31].owner==Board[34].owner)
    {
        ui->color_owner_7->setText(Board[31].owner->Name);
        Board[31].rent=2*Board[31].rent_default;
        Board[32].rent=2*Board[32].rent_default;
        Board[34].rent=2*Board[34].rent_default;

    }

    if(Board[37].owner==Board[39].owner)
    {
        ui->color_owner_8->setText(Board[37].owner->Name);
        Board[37].rent=2*Board[37].rent_default;
        Board[39].rent=2*Board[39].rent_default;

    }

    //check kardan inke player varshekaste shode ya na
    for(int i=0;i<tedad_player;i++)
    {
        if (players[i].out==false && players[i].Money<0)

            varshekastegi(i);

        if(players[i].out==false && players[i].Money<0)
        {
            players[i].out=true;
            ui->listWidget->item(i)->setText("LOST!");
            mohre[i]->hide();
            ingame_players--;
        }
    }
    // winner
    if(ingame_players==1)
    {
        for(int i=0;i<tedad_player;i++)
            if(players[i].out==false) QMessageBox::information(this,"Winner",players[i].Name+" barande shod!");
        QTime dieTime= QTime::currentTime().addSecs(3);
        while (QTime::currentTime() < dieTime)
            QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
        delete this;

    }
}


void MainWindow::on_pushButton_2_clicked()//home
{
    H->show();
    ui->pushButton->setEnabled(true);
    ui->pushButton_2->hide();



}
void MainWindow::on_pushButton_clicked()//start
{
    ui->pushButton->hide();
    ui->sell_btn->show();
    ui->build_btn->show();
    ui->Mortgage_btn->show();
    ui->ooj_btn->show();
    ui->getblock->show();
    ui->label_3->show();

    //enteqal dadan esm o tedad player az class home be mainwindow
    tedad_player =H->tedad_player_home;
    ingame_players=tedad_player;


    for(int i=0;i<tedad_player;i++)
    {
        players[i].Name=H->players_name_home[i];
    }

    //update kardan status har 1 sanie
    connect(update, SIGNAL(timeout()), this, SLOT(update_Status()));
    update  ->start(1000);

    QMessageBox::information(this,"ROLL DICE","baraye moshakhas kardan nobat be tedad player ha tas bendazid");

    ui->pushButton_3->setEnabled(true);


}


void MainWindow::on_pushButton_3_clicked()//roll dice
{
    ui->pushButton_3->hide();
    ui->pushButton_4->show();
    if(nobat==tedad_player)
    {
        nobat++;

    }
    if(nobat<tedad_player)//be tedad player ha tas mindaze
    {

        tw=new tas_window;
        tw->show();
        taas=tw->tas;
        players[nobat].tas_shuru=taas;
        if(nobat==tedad_player-1)
        {

            //moratab kardan tartib player ha

            int i,j;
            for (i=0; i<tedad_player; i++)
                for (j=i+1; j<tedad_player; j++)
                    if (players[i].tas_shuru < players[j].tas_shuru)    qSwap(players[i],players[j]);

            //show kardan mohre ha va rang haye player ha
            for(int i=0;i<tedad_player;i++) mohre[i]->show();
            for(int i=0;i<tedad_player;i++) rang[i]->show();

        }


        nobat++;
    }

    if(nobat==tedad_player+1)//                 loop nobat
    {

        //tas andakhtan
        if(players[turn].jailed==true) QMessageBox::information(this,"JAILED", "you are in jail!"); // check kone taraf too jail nabashe
        else{
            tw=new tas_window;
            tw->show();
            taas=tw->tas;
            players[turn].Block+=taas;

            QTime dieTime= QTime::currentTime().addSecs(2);
            while (QTime::currentTime() < dieTime)
                QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
            tw->hide();

            // anjam dadan action marboot be block
            aw=new action_window(this);
            aw->action(players[turn].Block,players,players[turn],Board,tedad_player,taas);




            //check kardan block ke az go rad shode dobare ya na
            if (players[turn].Block>=40)
            {
                players[turn].Block=players[turn].Block%40;
                players[turn].Money+=200;
            }

            if(tw->Dice1==tw->Dice2) doubl++;
            else doubl=0;
            if (doubl==3)
            {
                doubl=0;
                players[turn].Block=10;
                players[turn].jailed=true;
            }



            //harekat graphici
            move_mohre(mohre[turn],players[turn].Block);




            //payane nobat player
            if(doubl>0 )turn--;



        }

    }
}

void MainWindow::on_pushButton_4_clicked() //Done
{
    ui->pushButton_3->show();
    ui->pushButton_4->hide();

    turn++;
    if(turn==tedad_player)turn=0;
    if(players[turn].out==true) turn++;
    if(turn==tedad_player)turn=0;

    if(players[turn].out==true) turn++;
    if(turn==tedad_player)turn=0;

    if(players[turn].out==true) turn++;
    if(turn==tedad_player)turn=0;

    if(players[turn].out==true) turn++;
    if(turn==tedad_player)turn=0;

    if(players[turn].out==true) turn++;
    if(turn==tedad_player)turn=0;

    if(players[turn].out==true) turn++;
    if(turn==tedad_player)turn=0;




    // back ground list ro taqir mide ke maloom mishe kudum player dare bazi mikone
    ui->listWidget->item(turn)->setBackgroundColor(Qt::lightGray);
    int turn_qabli;//baraye taqir rang background be halate default
    if (turn>0) turn_qabli=turn-1;
    else turn_qabli=tedad_player-1;
    ui->listWidget->item(turn_qabli)->setBackgroundColor(Qt::white);
}


void MainWindow::on_Mortgage_btn_clicked()
{
    int block_target=ui->getblock->text().toInt();
    ui->getblock->clear();

    if(Board[block_target].owner!=&players[turn]) QMessageBox::warning(this,"","You are not the owner");
    else if( Board[block_target].mortgaged==false)
    {
        Board[block_target].mortgaged=true;
        players[turn].Money+=Board[block_target].mortgage;
    }
    else if( Board[block_target].mortgaged==true)
    {
        Board[block_target].mortgaged=false;
        players[turn].Money-=(Board[block_target].mortgage*110)/100;
    }
}

void MainWindow::on_build_btn_clicked()
{
    // board
    int block_T =ui->getblock->text().toInt();
    ui->getblock->clear();
    build(this,Board,block_T);

}

void MainWindow::on_ooj_btn_clicked()
{
    if(players[turn].jailed==false)QMessageBox::information(this,"","shoma dar jail nistid!");
    else
    {
        oojw=new ooj(this);
        oojw->getout(players[turn]);
    }

}

void MainWindow::on_sell_btn_clicked()
{

    sellw=new sell_window(this);
    sellw->Board_C=Board;
    sellw->Players_c=players;
    sellw->show();
}

void MainWindow::varshekastegi(int tartib)
{
    players[tartib].Money+=(players[tartib].value/2);
    players[tartib].value=0;
    for(int i=0;i<40;i++)
    {
        if(Board[i].owner==&players[tartib])
        {
            labels[i]->clear();
            Board[i].owner=0;
            Board[i].owned=false;
            Board[i].tedad_hotel=0;
            Board[i].tedad_khune=0;
        }
    }
}



