#include "ooj.h"
#include "ui_ooj.h"
#include<QMessageBox>
#include<QTime>
ooj::ooj(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ooj)
{
    ui->setupUi(this);
}

ooj::~ooj()
{
    delete ui;
}

void ooj::getout(Player &player)
{
    player_cp=&player;
    show();
}


void ooj::on_pay_clicked()
{
    player_cp->Money-=50;
    player_cp->jailed=false;
    hide();
}

void ooj::on_card_clicked()
{
    if(player_cp->ooj==false)QMessageBox::information(this,"card","shoma card out of jail nadarid!");
    else {
        player_cp->jailed=false;
        player_cp->ooj=false;
        hide();
    }
}

void ooj::on_roll_clicked()
{
    twj=new tas_window;   //tas andakhtan bare aval
    twj->show();
    if(twj->Dice1==twj->Dice2)
    {
        taas=twj->tas;
        player_cp->jailed=false;
        player_cp->Block+=taas;
        hide();
    }
    else   
    {
        QTime dieTime= QTime::currentTime().addSecs(1);
            while (QTime::currentTime() < dieTime)
                QCoreApplication::processEvents(QEventLoop::AllEvents, 100);

        twj=new tas_window; //tas andakhtan bare dovom
        twj->show();
        if(twj->Dice1==twj->Dice2)
        {

            taas=twj->tas;
            player_cp->jailed=false;
            player_cp->Block+=taas;

            hide();
        }
        else  //tas andakhtan bare sevom
        {
            QTime dieTime= QTime::currentTime().addSecs(1);
                while (QTime::currentTime() < dieTime)
                    QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
            twj=new tas_window;
            twj->show();
            if(twj->Dice1==twj->Dice2)
            {

                taas=twj->tas;
                player_cp->jailed=false;
                player_cp->Block+=taas;
                hide();
            }
            else
            {
                taas=twj->tas;
                player_cp->jailed=false;
                player_cp->Block+=taas;
                player_cp->Money-=50;
                hide();
            }
        }
    }
}
