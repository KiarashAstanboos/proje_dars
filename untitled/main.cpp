#include "mainwindow.h"

#include <QApplication>
#include<QMediaPlayer>
#include<QDebug>
#include<QTimer>



int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    //background music
    QMediaPlayer *background;
    background=new QMediaPlayer();
    background->setMedia(QUrl("qrc:/Energetic_Music_for_Board_Games_2kmAzSlogh0_140.mp3"));
    background->play();

    MainWindow w;
    w.show();

    return a.exec();


}
