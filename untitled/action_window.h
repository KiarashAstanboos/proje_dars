#ifndef ACTION_WINDOW_H
#define ACTION_WINDOW_H

#include <QDialog>
#include"melk_class.h"
#include"player_class.h"
namespace Ui {
class action_window;
}

class action_window : public QDialog
{
    Q_OBJECT

public:
    explicit action_window(QWidget *parent = nullptr);
    ~action_window();

    void action(int block,Player *players,Player &player,melk Board[],int tedad_player,int taas);
    int rent();//mohasebe mikone cheqad rent bayad pardakht beshe

     int chance_array[16];
    void pick_chance(Player &player, melk Board[]);
     int community_array[16];
    void pick_community(Player &player, melk Board[]);

    int Block;
    Player *players_copy;
    Player *player_copy;
    melk *Board_copy;

    int auction_participants;//tedad player ha
    bool auction_bool[8]; // che player haii az haraj enseraf dadan
    int auction_turn; // nobat kudum playere ke qeymat vared kone
    int winner; //kasi ke haraj ro borde
    int bids[8];//qeymati ke har player dade dakhel in rikhte mishe

    int tas;


private slots:
    void on_Buy_btn_clicked();

    void on_Cancel_btn_clicked();

    void on_auction_btn_clicked();

    void on_Bid_btn_clicked();

    void on_Okay_btn_clicked();

private:
    void sell(int block,Player &player,melk Board[],int price);


    Ui::action_window *ui;
};

#endif // ACTION_WINDOW_H
