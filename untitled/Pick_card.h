#ifndef PICK_CARD_H
#define PICK_CARD_H
#include"melk_class.h"

void pick_chance(Player &player, melk Board[])
{
    ui->Okay_btn->show();
    ui->Buy_btn->hide();
    ui->auction_btn->hide();
    show();
    int shomare=chance_array[0];

    for(int i=0;i<15;i++)
        chance_array[i]=chance_array[i+1]; //gozashtan card entekhab shode tahe list
    chance_array[15]=shomare;

    switch (shomare)
    {
    case 1:ui->frame->setStyleSheet("border-image:url(:/chance/chance cards/1.png)");

        player.Block=39;
        break;

    case 2:ui->frame->setStyleSheet("border-image:url(:/chance/chance cards/2.png)");
        player.Money+=200;
        player.Block=0;
        break;

    case 3:ui->frame->setStyleSheet("border-image:url(:/chance/chance cards/3.png)");
        if(player.Block>24) player.Money+=200;
        player.Block=24;

        break;

    case 4:ui->frame->setStyleSheet("border-image:url(:/chance/chance cards/4.png)");
        if(player.Block>11) player.Money+=200;
        player.Block=11;
        break;

    case 5:ui->frame->setStyleSheet("border-image:url(:/chance/chance cards/5.png)");
        // raftan be nazdik tarin RR
        int block_target;
        int minimum;
        for(int RR=5,minimum=50;RR<=45;RR+=5)
        {
            if (qFabs( RR-Block)<minimum)
            {
                minimum=qFabs( RR-Block);
                block_target=RR;
            }
        }
        player.Block=block_target;
        Block=block_target;

        // check kardan inke owner dare ya na va gereftan rent ya kharidan RR
        if(Board[block_target].owned==false)
        {
            ui->frame->setGeometry(0,10,250,370);
            ui->Buy_btn->show();
            ui->auction_btn->show();
            show();
            ui->frame->setStyleSheet("border-image:url(:/ownership/ownership/"+QString::number(Block)+".PNG)");
            ui->Buy_btn->setText("Buy for "+QString::number(Board[Block].price)+"$");
        }
        else
        {
            player.Money-=2*rent();
            Board[Block].owner->Money+=2*rent();
        }
        break;

    case 6:ui->frame->setStyleSheet("border-image:url(:/chance/chance cards/6.png)");
        // raftan be nazdik tarin RR
        // int block_target;
        // int minimum;
        for(int RR=5,minimum=50;RR<=45;RR+=5)
        {
            if (qFabs( RR-Block)<minimum)
            {
                minimum=qFabs( RR-Block);
                block_target=RR;
            }
        }
        player.Block=block_target;
        Block=block_target;

        // check kardan inke owner dare ya na va gereftan rent ya kharidan RR
        if(Board[block_target].owned==false)
        {
            ui->frame->setGeometry(0,10,250,370);
            ui->Buy_btn->show();
            ui->auction_btn->show();
            show();
            ui->frame->setStyleSheet("border-image:url(:/ownership/ownership/"+QString::number(Block)+".PNG)");
            ui->Buy_btn->setText("Buy for "+QString::number(Board[Block].price)+"$");
        }
        else
        {
            player.Money-=2*rent();
            Board[Block].owner->Money+=2*rent();
        }
        break;

    case 7:ui->frame->setStyleSheet("border-image:url(:/chance/chance cards/7.png)");
        // raftan be nazdik tarin utility
        int block_target_u;
        int min_u;
        for(int u=5,min_u=50;u<=45;u+=5)
        {
            if (qFabs( u-Block)<min_u)
            {
                min_u=qFabs( u-Block);
                block_target_u=u;
            }
        }
        player.Block=block_target_u;
        Block=block_target_u;

        // check kardan inke owner dare ya na va gereftan rent ya kharidan RR
        if(Board[block_target_u].owned==false)
        {
            ui->frame->setGeometry(0,10,250,370);
            ui->Buy_btn->show();
            ui->auction_btn->show();
            show();
            ui->frame->setStyleSheet("border-image:url(:/ownership/ownership/"+QString::number(Block)+".PNG)");
            ui->Buy_btn->setText("Buy for "+QString::number(Board[Block].price)+"$");
        }
        else
        {
            player.Money-=10*tas;
            Board[Block].owner->Money+=10*tas;
        }
        break;

    case 8:ui->frame->setStyleSheet("border-image:url(:/chance/chance cards/8.png)");
        player.Money+=50;
        break;

    case 9:ui->frame->setStyleSheet("border-image:url(:/chance/chance cards/9.png)");
        player.Block-=3;
        break;

    case 10:ui->frame->setStyleSheet("border-image:url(:/chance/chance cards/10.png)");
        player.ooj=true;
        break;

    case 11:ui->frame->setStyleSheet("border-image:url(:/chance/chance cards/11.png)");
        player.jailed=true;
        player.Block=10;
        break;

    case 12:ui->frame->setStyleSheet("border-image:url(:/chance/chance cards/12.png)");
        player.Money-=25*player.tedad_khune;
        player.Money-=100*player.tedad_hotel;
        break;

    case 13:ui->frame->setStyleSheet("border-image:url(:/chance/chance cards/13.png)");
        player.Money-=15;
        break;

    case 14:ui->frame->setStyleSheet("border-image:url(:/chance/chance cards/14.png)");
        if (player.Block>5) player.Money+=200;
        player.Block=5;

        break;

    case 15:ui->frame->setStyleSheet("border-image:url(:/chance/chance cards/15.png)");
        player.Money+=150;
        break;

    case 16:ui->frame->setStyleSheet("border-image:url(:/chance/chance cards/16.png)");
        player.Money-=(auction_participants-1)*50;
        for(int i=0;i<auction_participants;i++)
            if(players_copy[i].Name!=player.Name)
                players_copy[i].Money+=50;

        break;



    }
}

void pick_community(Player &player, melk Board[])
{
    ui->Okay_btn->show();
    ui->Buy_btn->hide();
    ui->auction_btn->hide();
    int shomare=community_array[0];
    show();
    for(int i=0;i<15;i++)
        community_array[i]=community_array[i+1]; //gozashtan card entekhab shode tahe list
    community_array[15]=shomare;
    switch (shomare)
    {
    case 1:ui->frame->setStyleSheet("border-image:url(:/community/community/1.png)");
        player.Money+=200;
        player.Block=0;
        break;

    case 2:ui->frame->setStyleSheet("border-image:url(:/community/community/2.png)");
        player.Money+=200;
        break;

    case 3:ui->frame->setStyleSheet("border-image:url(:/community/community/3.png)");
        player.Money-=200;
        break;

    case 4:ui->frame->setStyleSheet("border-image:url(:/community/community/4.png)");
        player.Money+=45;
        break;

    case 5:ui->frame->setStyleSheet("border-image:url(:/community/community/5.png)");
        player.Money+=(auction_participants-1)*50;
        for(int i=0;i<auction_participants;i++)
            if(players_copy[i].Name!=player.Name)
                players_copy[i].Money-=50;
        break;

    case 6:ui->frame->setStyleSheet("border-image:url(:/community/community/6.png)");
        player.ooj=true;
        break;

    case 7:ui->frame->setStyleSheet("border-image:url(:/community/community/7.png)");
        player.jailed=true;
        player.Block=10;
        break;

    case 8:ui->frame->setStyleSheet("border-image:url(:/community/community/8.png)");
        player.Money+=20;
        break;

    case 9:ui->frame->setStyleSheet("border-image:url(:/community/community/9.png)");
        player.Money+=100;
        break;

    case 10:ui->frame->setStyleSheet("border-image:url(:/community/community/10.png)");
        player.Money-=100;
        break;

    case 11:ui->frame->setStyleSheet("border-image:url(:/community/community/11.png)");
        player.Money-=150;
        break;

    case 12:ui->frame->setStyleSheet("border-image:url(:/community/community/12.png)");
        player.Money+=25;
        break;

    case 13:ui->frame->setStyleSheet("border-image:url(:/community/community/13.png)");
        player.Money+=100;
        break;

    case 14:ui->frame->setStyleSheet("border-image:url(:/community/community/14.png)");
        player.Money-=40*player.tedad_khune;
        player.Money-=115*player.tedad_hotel;
        break;

    case 15:ui->frame->setStyleSheet("border-image:url(:/community/community/15.png)");
        player.Money+=10;
        break;

    case 16:ui->frame->setStyleSheet("border-image:url(:/community/community/16.png)");
        player.Money+=100;
        break;
    }

}

#endif // PICK_CARD_H
